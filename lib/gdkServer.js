const http = require('http'),
  fs = require('fs'),
  path = require('path'),
  connect = require('connect'),
  serveStatic = require('serve-static'),
  bodyParser = require('body-parser'),
  request = require('request'),
  Webpack = require("webpack"),
  webpackMiddleware = require("webpack-dev-middleware");

const glProxy = require('./gitlabProxy.js'),
  GDKAPI = require('./gdkAPI'),
  webpackConfig = require("../webpack.config");

class GDKServer {
  constructor() {
    //Take a look for a config file in executing folder
    global.gdkConfig = {};
    global.currentTarget = 'https://gitlab.com';
    global.injectToolbar = true;
    global.redirectJS = false;
    global.redirectCSS = false;

    // Check if we are in dev or in running mode
    try {
      fs.accessSync(process.cwd() + '/toolbar');
      global.runningMode = 'DEVELOPMENT';
    } catch (e) {
      global.runningMode = 'SERVER';
    }

    console.log('GDK JS Starting up in ' + global.runningMode + ' Mode');


    const configPath = process.cwd() + '\\.gdkjsconfig';

    fs.watch(configPath, (eventType, filename) => {
      console.log(`GDK Config File was changed -> Reloading`);
      global.gdkConfig = JSON.parse(fs.readFileSync(configPath, 'utf8'));
      console.log('Loaded Config : ',global.gdkConfig);
    });

    fs.access(configPath, fs.constants.R_OK, (err) => {
      if (!err) {
        global.gdkConfig = JSON.parse(fs.readFileSync(configPath, 'utf8'));
        console.log(`Loaded successfully local GDK JS config (${configPath})`);
      } else {
        console.log('Create Custom Config by creating .gdkjsconfig file through "gdk-js init"');
      }

      console.log('GDK-JS Config : ', global.gdkConfig);

      const compiler = Webpack(webpackConfig);

      const app = connect();

      // Static Files
      app
        .use(bodyParser.json())
        .use('/gdk.js.assets', serveStatic(process.cwd(), {
          index: false,
          fallthrough: false
        }))
        .use('/gdk.js.toolbar', serveStatic(path.join(__dirname, '../toolbar'), {
          index: false
        }));

      new GDKAPI(app);

      if (global.runningMode === 'DEVELOPMENT') {
        // We add the Webpack dev Server only if we are developing the GDK
        app.use(webpackMiddleware(compiler, {
          // publicPath is required, whereas all other options are optional
          noInfo: false,
          // display no info to console (only warnings and errors)
          lazy: false,
          // switch into lazy mode
          // that means no watching, but recompilation on every request
          watchOptions: {
            aggregateTimeout: 300,
            poll: true
          },
          // watch options (only lazy: false)
          publicPath: "/gdk.js.toolbar.dist/",
          // public path to bind the middleware to
          // use the same as in webpack
          stats: {
            colors: true
          },
          // options for formating the statistics
        }))
      }

      // Init actual Proxy  
      app.use(glProxy(global.gdkConfig));

      http.createServer(app).listen(5050);
    });
  }
}

module.exports = GDKServer;
