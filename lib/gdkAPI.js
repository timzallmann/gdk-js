class GdkAPI {
  constructor(app) {
    app.use('/gdk.js.api/config', this.handleConfig);
  }

  handleConfig(req, res) {
    if (req.method === 'GET') {
      const infoObject = {};
      infoObject.currentTarget = global.currentTarget;
      infoObject.redirectJS = global.redirectJS;
      infoObject.redirectCSS = global.redirectCSS;

      res.end(JSON.stringify(infoObject));
    } else if (req.method === 'PUT') {
      if (req.body) {
        console.log('Save Body : ', req.body);

        global.currentTarget = req.body.currentTarget;
        global.redirectJS = req.body.redirectJS;
        global.redirectCSS = req.body.redirectCSS;

        res.end("Saved")
      } else {
        res.statusCode = 401;
        res.end('No data received');
      }
    } else {
      res.statusCode = 401;
      res.end("HTTP Method not supported")
    }

  }

}

module.exports = GdkAPI;
