const httpProxy = require('http-proxy'),
  request = require('request'),
  cheerio = require('cheerio'),
  urlPattern = require('url-pattern'),
  fs = require('fs'),
  path = require('path');

const ToolsInjector = require('./toolsInjector');

let serverOptions = {};

const cookieRequest = request.defaults({
  followRedirect: false,
  jar: true,
  time: true,
  headers: {
    Host: 'gitlab.com',
    Referer: 'https://gitlab.com/users/sign_in'
  }
});

//Cookie: '_mkto_trk=id:194-VVC-221&token:_mch-gitlab.com-1497280619325-72962; _pk_ref.1.e54d=%5B%22%22%2C%22%22%2C1497280623%2C%22https%3A%2F%2Fabout.gitlab.com%2F%22%5D; _pk_id.1.e54d=c196ca2255cabf1c.1497280623.1.1497280715.1497280623.; _pk_ses.1.e54d=*; _gitlab_session=49d3c9c9f1472c97c47ae48c5984be0d'

/**
 * 
 * Transforms the returned Page
 * 
 * @param {any} url 
 * @param {any} opts 
 * @param {any} req 
 * @param {any} res 
 */
function returnPage(url, opts, req, res) {
  cookieRequest({
    url: global.currentTarget + url,
    headers: {
      Host: 'gitlab.com',
      Referer: 'https://gitlab.com/users/sign_in'

    }
  }, function (error, response, body) {
    if (error) {
      res.statusCode = 404;
      return res.end("Selected Target not found");
    }
    console.log('Status : ' + response.statusCode + ':' + url + ' ', opts);
    //res.headers = response.headers;

    const $ = cheerio.load(body);
    let selectedModification = null;

    if (opts) {
      // Update the title
      if (opts.title) {
        $('title').text(opts.title);
        $('.title-container .title').text(opts.title);
      }

      // Do all Replacements
      if (opts.modifications) {
        Object.keys(opts.modifications).forEach((modificationKey) => {
          if ($(modificationKey).length) {
            const modification = opts.modifications[modificationKey];
            let content;
            // Either its a haml file
            if (modification.template) {
              content = 'Render Template ' + modification.template;
            } else if (modification.htmlTemplate) {
              content = fs.readFileSync(path.join(process.cwd(), modification.htmlTemplate)).toString();
            } else if (modification.html) {
              content = modification.html;
            }

            // Based on Mode do the modification
            if (modification.mode === 'append') {
              $(modificationKey).append(content);
            } else if (modification.mode === 'prepend') {
              $(modificationKey).prepend(content);
            } else {
              $(modificationKey).html(content)
            }
          } else {
            console.warn(`No elements found for replacement key '${modificationKey}' `)
          }
        });
      }

      // Inject JS or CSS Files into the page
      if (opts.injectFiles) {
        opts.injectFiles.forEach((fileToInject) => {
          console.log('Injecting : ' + fileToInject);
          if (fileToInject.indexOf('.js') > -1) {
            let srcPath = fileToInject.indexOf('http')===0 ? fileToInject : `/gdk.js.assets/${fileToInject}`;
            $('head').append(`<script type="text/javascript" src="${srcPath}"/>`);
          } else if (fileToInject.indexOf('.css') > -1) {
            let hrefPath = fileToInject.indexOf('http')===0 ? fileToInject : `/gdk.js.assets/${fileToInject}`;
            $('head').append(`<link rel="stylesheet" href="${hrefPath}"/>`);
          }

        });
      }
    }

    if (global.redirectJS) {
      $('head script').each(function (i, elem) {
        if ($(this).attr('src') && $(this).attr('src').indexOf('/assets/webpack/') > -1) {
          let fileName = $(this).attr('src').split('/')[3];
          let fileNameParts = fileName.split('.');
          fileNameParts.splice(1, 1);
          $(this).attr('src', `${global.gdkConfig.webpackServerUrl}/assets/webpack/${fileNameParts.join('.')}`);
        }
      });
    }

    if (global.redirectCSS) {
      $('head link[rel="stylesheet"]').each(function (i, elem) {
        if ($(this).attr('href').indexOf('/assets/application-') > -1 || $(this).attr('href').indexOf('/assets/print-') > -1) {
          $(this).attr('href', global.gdkConfig.webpackServerUrl + $(this).attr('href'));
        }
      });
    }

    ToolsInjector.injectTools($, opts, response.timingPhases);

    res.end($.html());
  });
}

/**
 * Base Proxy Server
 * 
 * @param {any} options 
 * @returns 
 */
function proxyServer(options) {
  console.log('Do The Proxy Setup');

  if (options) {
    serverOptions = options;
  }

  return function (req, res, next) {
    if (req.headers && req.headers.accept && req.headers.accept.indexOf('html') > -1) {
      if (req.url.startsWith('/gdk.js/') && serverOptions.emptyPages) {
        // Empty Page -> lets check our options
        const emptyUrl = req.url.toLowerCase().replace('/gdk.js/', '');
        if (serverOptions.emptyPages[emptyUrl]) {
          const emptyOption = serverOptions.emptyPages[emptyUrl];
          if (!emptyOption.basePage) emptyOption.bassePage = '/explore';
          returnPage(emptyOption.basePage, emptyOption, req, res);
        } else {
          res.statusCode = 404;
          res.end('GDK-JS : Empty Page route not found');
        }
      } else if (serverOptions.routes) {
        const selectedRoute = Object.keys(serverOptions.routes).find((route) => {
          const pattern = new urlPattern(route);
          console.log('Checking URL : ' + req.url + ' : ' + route + ' = ', pattern.match(req.url));
          return pattern.match(req.url);
        });

        if (selectedRoute) {
          // We have a replacement for an existing URL setup
          console.log('Found Route Matched : ', serverOptions.routes[selectedRoute], ' for URL ' + req.url);
          returnPage(req.url, serverOptions.routes[selectedRoute], req, res);
        } else {
          // All other pages
          returnPage(req.url, null, req, res);
        }
      } else {
        returnPage(req.url, null, req, res);
      }
    } else {
      //We just pipe it for all other files
      console.log('Pipe Proxy : ' + req.url);
      const x = cookieRequest(global.currentTarget + req.url)
      req.pipe(x);
      x.pipe(res);
    }

  }
};

module.exports = proxyServer;
