"use strict";

class ToolsInjector {
  static injectTools($, replacement, timings) {
    if (global.injectToolbar) {
      const infoObject = {};
      infoObject.currentTarget = global.currentTarget;
      infoObject.targets = global.gdkConfig.targets;
      infoObject.redirectJS = global.redirectJS;
      infoObject.redirectCSS = global.redirectCSS;
      infoObject.replacementUsed = replacement || null;
      infoObject.timingTotal = Math.round(timings.total);

      let injectStr = '';
      injectStr = injectStr + `<script type="text/javascript">let gdkInfo = ${JSON.stringify(infoObject)};</script>`;
      injectStr = injectStr + `<link rel="stylesheet" href="/gdk.js.toolbar/css/gdk.js.toolbar.css"/>`;
      if (global.runningMode === 'SERVER') {
        injectStr = injectStr + `<script type="text/javascript" src="/gdk.js.toolbar/dist/gdk.js.toolbar-build.js"/>`;
      } else {
        injectStr = injectStr + `<script type="text/javascript" src="/gdk.js.toolbar.dist/gdk.js.toolbar-build.js"/>`;
      }
      $('head').append(injectStr);

      $('body').append('<div id="gdk-js-toolbar"><div id="gdk-js-toolbar-inner"></div>');
    }
  }
}

module.exports = ToolsInjector;
