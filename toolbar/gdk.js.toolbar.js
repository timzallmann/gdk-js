import Vue from 'vue'
import ToolbarApp from './ToolbarApp.vue'

document.addEventListener('DOMContentLoaded', () => new Vue({
  el: '#gdk-js-toolbar',
  render: h => h(ToolbarApp)
}));
